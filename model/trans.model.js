const mongoose = require('mongoose')
const Joi = require('joi')
const Schema = mongoose.Schema
const timestamps = require('mongoose-timestamp')



var transSchema = Schema({
    cardId: {
        type: String,
        require: true
    },
    initialBalance: {
        type: Number,
        required: true
    },
    transactionFare: {
        type: Number,
        required: false
    },
    newBalance: {
        type: Number,
        required: false
    }
});
transSchema.plugin(timestamps, {
    createdAt: 'registered_at',
    updatedAt: 'updated_at'
})

const trans = mongoose.model('Trans', transSchema);

module.exports.Trans = trans