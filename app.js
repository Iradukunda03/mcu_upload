const express = require("express")
require("./config/db")
const app = express()
const path = require("path")
const cors = require("cors")

app.use(cors());
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

const {
    TransRoute
} = require('./routes/trans.routes')
const port = process.env.PORT || 8080

const host = process.env.NODE_ENV === "production" ? process.env.PROD_URL : `localhost:${port}`

app.use('/api/Transaction', TransRoute)

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'))
})


app.listen(port, () => console.log(`Listening on port ${port}..`))