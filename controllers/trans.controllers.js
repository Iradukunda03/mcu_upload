const {
    Trans
} = require('../model/trans.model')
require("../config/db")

/**
 * Register a transaction
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
exports.createTransaction = async (req, res) => {

    try {
            
        const newTrans = new Trans({
          cardId: req.body.cardId,
          initialBalance: req.body.initialBalance,
          transactionFare: req.body.transactionFare,
          newBalance: req.body.newBalance
        }) 

        const trans = await newTrans.save()
        return res.status(201).json({
            'message': 'Transaction successfull..!!!',
            'Transaction': trans
        })
    } catch (err) {
        console.log(err.stack)
        res.send(err).status(500)
    }
}


/**
 * All transaction on the card
 * @param req
 * @param res
 * @returns {Promise<*>}
 * 
 */
exports.getAllTransaction = async (req, res) => {
    try {
        const card = await Trans.find()
        return res.status(200).json(card)
    } catch (e) {
        return res.send(e).status(500)
    }
}