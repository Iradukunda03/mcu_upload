const mongoose = require("mongoose");
const config=require('config');
const DB =config.get('mongoURI');

const connectDB = async () => {
  try {
    await mongoose.connect(DB, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
    });
    console.log("MongoDB is Connected...");
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

connectDB();
