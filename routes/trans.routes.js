const express = require('express');
const router = express.Router();
const controllers = require('../controllers/trans.controllers');


router.post('/',controllers.createTransaction)

router.get('/',controllers.getAllTransaction)

exports.TransRoute= router;

