 import React, {
     useState,
     useEffect
 } from 'react'
 import axios from '../axios'
 
 const Transaction = () => {

         const [transactions, setTransactions] = useState([]);
         useEffect(() => {
             async function loadTransactions() {

                 await axios.get("/").then((res) => {
                     setTransactions(res.data)
                 }).catch((error) => {
                     console.log(error)
                 })
             }
             loadTransactions()
         }, [])
         return ( < div >
           
             <div className="table-responsive">
                     <table class="table table-hover">
                                 <thead>
                                     <tr>
                                     <th scope="col">UUID CARD</th>
                                     <th scope="col">TRANS_FARE</th>
                                     <th scope="col">NEW_BALANCE</th>
                                     </tr>
                                 </thead>
                                 
                                 <tbody>
            
             {transactions.map(transaction=>(
                          <tr>
                            <td key={transaction._id} >{transaction.cardId}</td>
                            <td key={transaction._id} >{transaction.transactionFare}</td>
                            <td key={transaction._id} >{transaction.newBalance}</td>
                          </tr>  
                 )
             )}
                    
                    
                </tbody>
                </table>

             </div>           
             </div>
             
             )
         }
         export default Transaction;
